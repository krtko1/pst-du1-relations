from __future__ import annotations
from typing import Tuple, Any
from pyrsistent import PClass, PSet, pset, field, s


class Relation(PClass):
    """Class which stores relation above set of arguments."""

    arg: PSet[Any] = field(type=PSet, mandatory=True)
    rel: PSet[Tuple[Any, Any]] = field(type=PSet, initial=pset())

    def __contains__(self, pair: Tuple[Any, Any]) -> bool:
        """Check if x->y (pair) is in relation."""
        return pair in self.rel

    def contains(self, pair: Tuple[Any, Any]) -> bool:
        """Check if x->y (pair) is in relation."""
        return pair in self

    def add(self, pair: Tuple[Any, Any]) -> Relation:
        """Add x->y (pair) to relation."""
        return (
            Relation(arg=self.arg, rel=self.rel.union([pair]))
            if pair[0] in self.arg and pair[1] in self.arg
            else self
        )

    def remove(self, pair: Tuple[Any, Any]) -> Relation:
        """If exist remove x->y (pair) to relation."""
        return Relation(arg=self.arg, rel=self.rel - s(pair))

    def union(self, second_relation: Relation) -> Relation:
        """Union of two relations. Also make union of arguments."""
        return Relation(
            arg=self.arg.union(second_relation.arg),
            rel=self.rel.union(second_relation.rel),
        )

    def intersection(self, second_relation: Relation) -> Relation:
        """Intersecion of two relations. Also make union of arguments."""
        return Relation(
            arg=self.arg.union(second_relation.arg),
            rel=self.rel.intersection(second_relation.rel),
        )

    def subtraction(self, second_relation: Relation) -> Relation:
        """Substract given relation from it self. Also make union of arguments."""
        return Relation(
            arg=self.arg.union(second_relation.arg), rel=self.rel - second_relation.rel
        )

    def inverse(self) -> Relation:
        """Make inverse relation."""
        return Relation(arg=self.arg, rel=pset([(b, a) for a, b in self.rel]))

    def composition(self, second_relation: Relation) -> Relation:
        """Make composition of two relations.

        (make a -> c from a->b->c).
        Also make union of arguments.
        """
        return Relation(
            arg=self.arg.union(second_relation.arg),
            rel=pset(
                [
                    (a, z)
                    for a, b2 in self.rel
                    for b1, z in second_relation.rel
                    if b1 == b2
                ]
            ),
        )

    def is_reflexive(self) -> bool:
        """Check if relation is reflexive."""
        return self.rel.issuperset(pset([(x, x) for x in self.arg]))

    def is_symmetric(self) -> bool:
        """Check if relation is symmetric."""
        return self.rel == self.inverse().rel

    def is_transitive(self) -> bool:
        """Check if relation is transitive."""
        return self.rel.issuperset(
            pset([(a, z) for a, b1 in self.rel for b2, z in self.rel if b1 == b2])
        )

    def reflexive_transitive_closure(self) -> Relation:
        """Make reflexive transitive closure of relation."""
        ret = self.rel.union([(x, x) for x in self.arg])
        temp = ret.union([(a, z) for a, b1 in ret for b2, z in ret if b1 == b2])
        while temp != ret:
            ret = temp
            temp = ret.union([(a, z) for a, b1 in ret for b2, z in ret if b1 == b2])
        return Relation(arg=self.arg, rel=ret)
