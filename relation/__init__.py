from __future__ import annotations
from .relation_class import Relation
from pyrsistent import PSet
from typing import Any

__all__ = ["Relation", "get_relation_class"]
__author__ = "Matej Novota"
__copyright__ = "Copyright 2020, Matej Novota"
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Matej Novota"
__email__ = "matej.novota@gmail.com"
__status__ = "Production"


def get_relation_class(arguments: PSet[Any]) -> Relation:
    return Relation(arg=arguments)
