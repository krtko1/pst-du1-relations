from relation import __version__, Relation, get_relation_class
from pyrsistent import pset


def test_version():
    assert __version__ == "0.1.0"


class TestSimpleRelation:
    arg = pset([1, 2, 3])
    rel = get_relation_class(arg)
    check_rel = Relation(arg=arg, rel=pset([(1, 2)]))
    inverse_rel = Relation(arg=arg, rel=pset([(2, 1)]))

    bigger_rel = Relation(arg=arg, rel=pset([(1, 2), (2, 3)]))
    another_rel = Relation(arg=arg, rel=pset([(1, 2), (2, 1)]))
    union_rel = Relation(arg=arg, rel=pset([(1, 2), (2, 3), (2, 1)]))
    composition_rel = Relation(arg=arg, rel=pset([(1, 1)]))

    reflexive_rel = Relation(arg=arg, rel=pset([(1, 1), (2, 2), (3, 3)]))

    full_rel = Relation(
        arg=arg, rel=pset([(x, y) for x in [1, 2, 3] for y in [1, 2, 3]])
    )

    def test_success_contains(self):
        assert (1, 2) in self.check_rel

    def test_unsuccess_contains(self):
        assert (2, 1) not in self.check_rel

    def test_success_add(self):
        assert self.rel.add((1, 2)) == self.check_rel

    def test_add_same(self):
        assert self.check_rel.add((1, 2)) == self.check_rel

    def test_unsuccess_add(self):
        assert self.rel.add((11, 2)) == self.rel

    def test_success_remove(self):
        assert self.check_rel.remove((1, 2)) == self.rel

    def test_unsuccess_remove(self):
        assert self.rel.remove((1, 2)) == self.rel

    def test_union(self):
        assert self.bigger_rel.union(self.another_rel) == self.union_rel

    def test_intersection(self):
        assert self.bigger_rel.intersection(self.another_rel) == self.check_rel

    def test_subtraction(self):
        assert self.union_rel.subtraction(self.bigger_rel) == self.inverse_rel

    def test_inverse(self):
        assert self.inverse_rel.inverse() == self.check_rel

    def test_composition(self):
        assert self.bigger_rel.composition(self.another_rel) == self.composition_rel

    def test_is_reflexive(self):
        assert self.reflexive_rel.union(self.union_rel).is_reflexive()

    def test_is_not_reflexive(self):
        assert not self.composition_rel.is_reflexive()

    def test_is_symmetric(self):
        assert self.another_rel.is_symmetric()

    def test_is_not_symmetric(self):
        assert not self.union_rel.is_symmetric()

    def test_is_transitive(self):
        assert self.another_rel.union(self.reflexive_rel).is_transitive()

    def test_is_not_transitive(self):
        assert not self.bigger_rel.is_transitive()

    def test_reflexive_transitive_closure(self):
        assert self.union_rel.reflexive_transitive_closure() == self.full_rel.remove(
            (3, 1)
        ).remove((3, 2))
